package com.example.jomhikeapp.Remote;

import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.Model.User;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IMyAPI {

    //http://localhost:5000/api/register
    @Headers("Content-Type: application/json")
    @POST("api/register")
    Observable<String> registerUser(@Body User user);

    @POST("api/login")
    Observable<String> loginUser(@Body User user);

    @GET("/api/user/742d5cae-6184-4a32-a58d-36e23febf1a3")
    Observable<String> getAllUsers();

    @GET("/api/event/2")
    Call<String> getEventById();

    @GET("/api/event")
    Call <List<Event>> getAllEvents();
}
