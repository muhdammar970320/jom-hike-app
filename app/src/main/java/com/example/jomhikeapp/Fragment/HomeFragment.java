package com.example.jomhikeapp.Fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.jomhikeapp.Adapter.EventAdapter;
import com.example.jomhikeapp.Constants;
import com.example.jomhikeapp.Activity.EventDetailActivity;
import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.Model.Mountain;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Remote.IMyAPI;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import dmax.dialog.SpotsDialog;
import io.reactivex.disposables.CompositeDisposable;

public class HomeFragment extends Fragment {
    IMyAPI iMyAPI;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    View rootView;
    private RecyclerView mRecyclerView;
    private EventAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RequestQueue mQueue;
    public static String TAG = "HomeFragment";
    public static ArrayList<Event> mEventsList = new ArrayList<>();
    AlertDialog dialog = null;
    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        Log.d(TAG, "onCreateView");
        dialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .build();
        dialog.show();
        buildRecyclerView();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void jsonParse() {

        String url = Constants.BASE_URL + "/api/event";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray jsonArray = response.getJSONArray("events");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject event = jsonArray.getJSONObject(i);
                        int Id = event.getInt("id");
                        String Name = event.getString("name");
                        String Location = event.getString("location");
                        String State = event.getString("state");
                        String OrganizerName = event.getString("organizerName");
                        String OrganizerRegistrationNumber = event.getString("organizerRegistrationNumber");
                        String Description = event.getString("description");
                        Date StartDate = Constants.dateFormat.parse(event.getString("startDate"));
                        Date EndDate = Constants.dateFormat.parse(event.getString("endDate"));
                        String Status = event.getString("status");
                        UUID UserId = UUID.fromString(event.getString("userId"));
                        JSONObject mountain = event.getJSONObject("mountain");
                        int mountainId = mountain.getInt("id");
                        String mountainName = mountain.getString("name");
                        int mountainHeightFeet = mountain.getInt("heightFeet");
                        int mountainHeightMeter = mountain.getInt("heightMeter");
                        String mountainState = mountain.getString("state");
                        Double averageDifficulty = mountain.getDouble("averageDifficulty");
                        Double averageRating = mountain.getDouble("averageDifficulty");

                        Mountain mMountain = new Mountain(mountainId,  mountainName,  mountainHeightFeet,  mountainHeightMeter,  mountainState,  averageDifficulty,  averageRating);
                        Event eventClass = new Event(Id, Name, Location, State, OrganizerName, OrganizerRegistrationNumber, Description, StartDate, EndDate, Status, UserId, mMountain);
                        mEventsList.add(eventClass);

//                        Toast.makeText(getActivity(), Name, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, eventClass.toString());
                    }

                    mAdapter.setEvent(mEventsList);
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialog.dismiss();
            }
        });
        mQueue.add(request);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "OnStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");
        mRecyclerView.getRecycledViewPool().clear();
        mEventsList.clear();
        mAdapter.notifyDataSetChanged();
        mQueue = Volley.newRequestQueue(getActivity());
        jsonParse();

    }
    public void buildRecyclerView(){
        mRecyclerView = rootView.findViewById(R.id.eventRecyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new EventAdapter(getActivity(), mEventsList);
//        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new EventAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                changeItem(position);
            }
        });
    }
    public void changeItem(int position){
        Intent detailIntent = new Intent(getActivity(), EventDetailActivity.class);
        Event selectedEvent = mEventsList.get(position);
        detailIntent.putExtra("Event", selectedEvent);
        startActivity(detailIntent);
//        mAdapter.notifyItemChanged(position);
    }
}
