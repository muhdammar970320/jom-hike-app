package com.example.jomhikeapp.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Utils.UserSharedPreferences;

public class ProfileFragment extends Fragment {
    TextView tv_profile_username, tv_profile_first_name, tv_profile_lastname, tv_profile_phone, tv_profile_email;
    View viewRoot;
    private SharedPreferences sharedPreferences;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.fragment_profile, container, false);
        tv_profile_username = viewRoot.findViewById(R.id.tv_profile_username);
        tv_profile_first_name = viewRoot.findViewById(R.id.tv_profile_first_name);
        tv_profile_lastname = viewRoot.findViewById(R.id.tv_profile_last_name);
        tv_profile_phone = viewRoot.findViewById(R.id.tv_profile_phone);
        tv_profile_email = viewRoot.findViewById(R.id.tv_profile_email);

        sharedPreferences = getActivity().getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);

        tv_profile_username.setText(sharedPreferences.getString(UserSharedPreferences.KEY_NAME,null));
        tv_profile_first_name.setText(sharedPreferences.getString(UserSharedPreferences.KEY_FNAME,null));
        tv_profile_lastname.setText(sharedPreferences.getString(UserSharedPreferences.KEY_LNAME,null));
        tv_profile_phone.setText(sharedPreferences.getString(UserSharedPreferences.KEY_PHONE,null));
        tv_profile_email.setText(sharedPreferences.getString(UserSharedPreferences.KEY_EMAIL,null));

        return viewRoot;

    }
}
