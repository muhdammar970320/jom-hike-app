package com.example.jomhikeapp.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Utils.UserSharedPreferences;

public class ProfileActivity extends AppCompatActivity {
    TextView tv_profile_username, tv_profile_first_name, tv_profile_lastname, tv_profile_phone, tv_profile_email;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        getSupportActionBar().setTitle("Profile");

        toolbar.setNavigationIcon(R.drawable.back_arrow); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });
        tv_profile_username = findViewById(R.id.tv_profile_username);
        tv_profile_first_name = findViewById(R.id.tv_profile_first_name);
        tv_profile_lastname = findViewById(R.id.tv_profile_last_name);
        tv_profile_phone = findViewById(R.id.tv_profile_phone);
        tv_profile_email = findViewById(R.id.tv_profile_email);

        sharedPreferences = getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);

        tv_profile_username.setText(sharedPreferences.getString(UserSharedPreferences.KEY_NAME,null));
        tv_profile_first_name.setText(sharedPreferences.getString(UserSharedPreferences.KEY_FNAME,null));
        tv_profile_lastname.setText(sharedPreferences.getString(UserSharedPreferences.KEY_LNAME,null));
        tv_profile_phone.setText(sharedPreferences.getString(UserSharedPreferences.KEY_PHONE,null));
        tv_profile_email.setText(sharedPreferences.getString(UserSharedPreferences.KEY_EMAIL,null));

    }
}
