package com.example.jomhikeapp.Activity;

import android.app.AlertDialog;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jomhikeapp.Fragment.HomeActivity;
import com.example.jomhikeapp.Model.User;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Remote.IMyAPI;
import com.example.jomhikeapp.Remote.RetrofitClient;
import com.example.jomhikeapp.Test.TestActivity;
import com.example.jomhikeapp.Utils.UserSharedPreferences;

import org.json.JSONObject;

import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    IMyAPI iMyAPI;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    EditText edt_user_name, edt_password;
    Button btn_login, btn_recyclerview, btn_login2, btn_test_activity;
    TextView txt_account;
    public static String TAG = "MainActivity";
    // User Session Manager Class
    UserSharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Create user session
        session = new UserSharedPreferences(getApplicationContext());

        if (session.checkLogin()) {
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);
        }
        //Init API
        iMyAPI = RetrofitClient.getInstance().create(IMyAPI.class);

        //Testing
        btn_test_activity = findViewById(R.id.btn_test_activity);

        btn_test_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, TestActivity2.class);
//                startActivity(intent);
            }
        });
        //ViewText
        edt_user_name = findViewById(R.id.edt_user_name);
        edt_password = findViewById(R.id.edt_password);
        btn_login = findViewById(R.id.btn_login);
        txt_account = findViewById(R.id.txt_account);
        btn_recyclerview = findViewById(R.id.btn_recyclerview);
        btn_login2 = findViewById(R.id.btn_login2);

        btn_recyclerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TestActivity.class);
                startActivity(intent);
            }
        });
        btn_login2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        //Event
        txt_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog = new SpotsDialog.Builder()
                        .setContext(MainActivity.this)
                        .build();
                dialog.show();

                User user = new User(edt_user_name.getText().toString(), edt_password.getText().toString(), "");

                compositeDisposable.add(iMyAPI.loginUser(user).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {


                                if (s.contains("Wrong User or Password")) {
                                    Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                } else if (s.contains("User is not existing in Database")) {
                                    Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                } else {
                                    JSONObject jsonObj = new JSONObject(s);
                                    Log.d(TAG, jsonObj.getString("Id"));
                                    Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                    //Convert string to JSON
                                    String userId = jsonObj.getString("Id");
                                    String firstName = jsonObj.getString("FirstName");
                                    String lastName = jsonObj.getString("LastName");
                                    String phoneNumber = jsonObj.getString("PhoneNumber");
                                    String email = jsonObj.getString("Email");

                                    session.createUserLoginSession(userId, firstName, lastName, phoneNumber, email, edt_user_name.getText().toString(),
                                            edt_password.getText().toString());
                                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                    // Add new Flag to start new Activity
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }

                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                dialog.dismiss();
                                Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }));

            }
        });
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}
