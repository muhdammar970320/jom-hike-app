package com.example.jomhikeapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.jomhikeapp.Fragment.HomeActivity;
import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationResult;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import java.util.Arrays;

public class MapsOfferRideActivity extends FragmentActivity implements OnMapReadyCallback {
    AutocompleteSupportFragment autocompleteFragment;
    Button btn_save_location;
    VerticalSeekBar mSeekBar;
    private GoogleMap mMap;
    private static String TAG = MapsOfferRideActivity.class.getSimpleName();
    private FusedLocationProviderClient client;
    Double mLatitude;
    Double mLongitude;
    LatLng mSelectedLocation;
    Event mEvent;
    private static final int MY_PERMISSION_REQUEST_CODE = 7192;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_offer_ride);
        client = LocationServices.getFusedLocationProviderClient(this);
        Intent intent = getIntent();
        mEvent = intent.getParcelableExtra("Event");
        String apiKey = getString(R.string.google_maps_key);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
//            PlacesClient placesClient = Places.createClient(this);
        }
        // Initialize the AutocompleteSupportFragment.
       autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);
        setAutocompleteFragment();
        btn_save_location = findViewById(R.id.btn_save_map_offer_ride);
        btn_save_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });
        mSeekBar = findViewById(R.id.verticalSeekBar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mMap.animateCamera(CameraUpdateFactory.zoomTo(progress),2000,null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        currentLocation();

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
            @Override
            public boolean onMyLocationButtonClick()
            {
                mMap.clear();
                LatLng loc = new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude());
                mMap.addMarker(new MarkerOptions().position(loc));
                if(mMap != null){
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                }
                Toast.makeText(MapsOfferRideActivity.this, String.format("%f : %f",
                        mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude()),
                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.3f));
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(latLng.toString())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                mSelectedLocation = latLng;
                Toast.makeText(getApplicationContext(),
                        "New marker added@" + latLng.toString(), Toast.LENGTH_LONG)
                        .show();
            }
        });
    }
    private void setAutocompleteFragment() {
        // Specify the types of place data to return.
        assert autocompleteFragment != null;
        autocompleteFragment.setCountry("MY");
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.NAME,Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " +
                        place.getName() + ", " +
                        place.getId() +", "+
                        place.getLatLng().latitude +", "+
                        place.getLatLng().longitude);
                String name = place.getName();
                // Creating a marker
                final MarkerOptions markerOptions = new MarkerOptions();

                Toast.makeText(getApplicationContext(), String.valueOf(place.getLatLng()), Toast.LENGTH_LONG).show();
                // Setting the position for the marker
                markerOptions.position(place.getLatLng());

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(name);

                // Clears the previously touched position
                mMap.clear();


                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        // Animating to the touched position
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 13.3f));

                        // Placing a marker on the touched position
                        mMap.addMarker(markerOptions);
                        mSelectedLocation = place.getLatLng();
                    }
                });
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

    }
    public void currentLocation(){
        if (ActivityCompat.checkSelfPermission(MapsOfferRideActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MapsOfferRideActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsOfferRideActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            client.getLastLocation().addOnSuccessListener(MapsOfferRideActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mMap.setMyLocationEnabled(true);
                    if (location != null) {
                        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.addMarker(new MarkerOptions()
                                .position(currentLocation)
                                .title("You")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 13.0f));
                        Log.d(TAG, location.getLatitude() + " " + location.getLongitude());
                        mLatitude = location.getLatitude();
                        mLongitude = location.getLongitude();
                        mSelectedLocation = currentLocation;

                    }
                }
            });
        }
    }
    private void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Confirmation");
            dialog.setMessage("Are you confirm to save this location?");
            dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,
                                    int which) {
                    Intent detailIntent = new Intent(MapsOfferRideActivity.this, OfferRideActivity.class);
                    detailIntent.putExtra("selectedLocation", mSelectedLocation.latitude+","+mSelectedLocation.longitude);
                    detailIntent.putExtra("Event", mEvent);
                    startActivity(detailIntent);
                }
            });

            dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    currentLocation();
                }
                break;

        }

    }
    @Override
    public void onBackPressed() {
        Intent detailIntent = new Intent(MapsOfferRideActivity.this, OfferRideActivity.class);
        detailIntent.putExtra("Event", mEvent);
        startActivity(detailIntent);
        finish();
    }

    }





