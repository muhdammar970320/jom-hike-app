package com.example.jomhikeapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jomhikeapp.Model.Ride;
import com.example.jomhikeapp.Model.Vehicle;
import com.example.jomhikeapp.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

public class RideDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static String TAG ="RideDetailsActivity";
    private Ride mRide;
    private static final int MY_PERMISSION_REQUEST_CODE = 7192;
    private FusedLocationProviderClient client;
    private GoogleMap mMap;
    ImageView tv_card_ride_details_ic_profile;
    private TextView tv_card_ride_details_first_name,tv_card_ride_details_last_name, tv_card_ride_details_car_brand, tv_card_ride_details_seat_available;
    Double mLatitude;
    Double mLongitude;
    LatLng mSelectedLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        Intent intent = getIntent();
        mRide = intent.getParcelableExtra("Ride");
        client = LocationServices.getFusedLocationProviderClient(this);
        Log.d(TAG, "Ride details: "+mRide.toString());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tv_card_ride_details_first_name = findViewById(R.id.tv_card_ride_details_first_name);
        tv_card_ride_details_last_name = findViewById(R.id.tv_card_ride_details_last_name);
        tv_card_ride_details_car_brand = findViewById(R.id.tv_card_ride_details_car_brand);
        tv_card_ride_details_seat_available = findViewById(R.id.tv_card_ride_details_seat_available);
        tv_card_ride_details_ic_profile = findViewById(R.id.tv_card_ride_details_ic_profile);

        tv_card_ride_details_first_name.setText(mRide.getVehicle().getUser().getFirstName());
        tv_card_ride_details_last_name.setText(mRide.getVehicle().getUser().getLastName());
        tv_card_ride_details_car_brand.setText(mRide.getVehicle().getBrand());
        tv_card_ride_details_seat_available.setText(mRide.getSeatAvailable()+"");
        tv_card_ride_details_ic_profile.setImageResource(R.drawable.ic_profile);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        currentLocation();
        setMarkerDriver();

    }
    public void currentLocation(){
        if (ActivityCompat.checkSelfPermission(RideDetailsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(RideDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RideDetailsActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            client.getLastLocation().addOnSuccessListener(RideDetailsActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
//                    mMap.setMyLocationEnabled(true);
                    if (location != null) {
                        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.addMarker(new MarkerOptions()
                                .position(currentLocation)
                                .title("You")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 10.0f));
                        Log.d(TAG, location.getLatitude() + " " + location.getLongitude());
                        mLatitude = location.getLatitude();
                        mLongitude = location.getLongitude();
                        mSelectedLocation = currentLocation;

                    }
                }
            });
        }
    }
    public void setMarkerDriver(){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mRide.getDestinationLatitude(), mRide.getDestinationLongitude()))
                    .title(mRide.getVehicle().getUser().getFirstName()));

        }
    }

