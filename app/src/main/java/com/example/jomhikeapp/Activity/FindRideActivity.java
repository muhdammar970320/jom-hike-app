package com.example.jomhikeapp.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.jomhikeapp.Adapter.EventAdapter;
import com.example.jomhikeapp.Adapter.RideAdapter;
import com.example.jomhikeapp.Constants;
import com.example.jomhikeapp.Model.Mountain;
import com.example.jomhikeapp.Model.Ride;
import com.example.jomhikeapp.Model.UserEvent;
import com.example.jomhikeapp.Model.Vehicle;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Utils.DateDeserializer;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class FindRideActivity extends AppCompatActivity implements OnMapReadyCallback {
    private RecyclerView mRecyclerView;
    private RideAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RequestQueue mQueue;
    private GoogleMap mMap;
    private FusedLocationProviderClient client;
    Double mLatitude;
    Double mLongitude;
    LatLng mSelectedLocation;
    private static final int MY_PERMISSION_REQUEST_CODE = 7192;
    String mEventId;
    Ride mRide;
    public static ArrayList<Ride> mRideList = new ArrayList<>();
    public static String TAG = "FindRideActivity";
    AlertDialog dialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_ride);

        //Init client
        client = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Intent intent = getIntent();
        mEventId = intent.getStringExtra("eventId");
        mQueue = Volley.newRequestQueue(this);
        //Loading spinner
        dialog = new SpotsDialog.Builder()
                .setContext(this)
                .build();
        dialog.show();

        //Build recyclerview
        buildRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");
        mRecyclerView.getRecycledViewPool().clear();
        mRideList.clear();
        mAdapter.notifyDataSetChanged();
        mQueue = Volley.newRequestQueue(this);

        //Get request from server
        getRideJson();
    }

    private void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.findRideRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RideAdapter(this, mRideList);
//        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RideAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                changeItem(position);
            }
        });

    }
    public void changeItem(int position){
        Intent detailIntent = new Intent(this, RideDetailsActivity.class);
        Ride selectedRide = mRideList.get(position);
        detailIntent.putExtra("Ride", selectedRide);
        startActivity(detailIntent);
//        mAdapter.notifyItemChanged(position);
    }
    private void getRideJson() {

        String url = Constants.BASE_URL + "/api/ride/event/"+mEventId;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    //Set Date to follow GMT time
                    gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
                    final Gson gson = gsonBuilder.create();

                    JSONArray jsonArray = response.getJSONArray("rides");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject ride = jsonArray.getJSONObject(i);

                        //This code below is unused right now but can use for debugging in future
                        int id = ride.getInt("id");
                        Date rideStartTime = Constants.dateFormat.parse(ride.getString("rideStartTime"));
                        Double destinationLongitude =  Double.parseDouble(ride.getString("destinationLongitude"));
                        Double destinationLatitude = Double.parseDouble(ride.getString("destinationLatitude"));
                        int seatAvailable = Integer.parseInt(ride.getString("seatAvailable"));
                        Double totalCost = Double.parseDouble(ride.getString("totalCost"));
                        Double totalDistance = Double.parseDouble(ride.getString("totalDistance"));
                        Date createdAt = Constants.dateFormat.parse(ride.getString("createdAt"));

                        //Convert Ride json Object to Ride class
                        Ride rideObject = gson.fromJson(ride.toString(), Ride.class);
                        Log.d(TAG,"Ride object successfully get from server "+rideObject.toString());
                        mRideList.add(rideObject);
                    }
                    setMarkerDriver();
                    mAdapter.setRide(mRideList);
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialog.dismiss();
            }
        });
        mQueue.add(request);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        currentLocation();
    }
    public void currentLocation(){
        if (ActivityCompat.checkSelfPermission(FindRideActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(FindRideActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FindRideActivity.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            //Get current location of the rider
            client.getLastLocation().addOnSuccessListener(FindRideActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mMap.setMyLocationEnabled(true);
                    if (location != null) {
                        LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.addMarker(new MarkerOptions()
                                .position(currentLocation)
                                .title("You")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 13.0f));

                        Log.d(TAG, "Current location: "+location.getLatitude() + " : " + location.getLongitude());

                        //This code is unused but can used for debugging
                        mLatitude = location.getLatitude();
                        mLongitude = location.getLongitude();
                        mSelectedLocation = currentLocation;

                    }
                }
            });
        }
    }
    public void setMarkerDriver(){
        for (int i =0; i<mRideList.size(); i++){
            Ride ride = mRideList.get(i);
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(ride.getDestinationLatitude(), ride.getDestinationLongitude()))
                    .title(ride.getVehicle().getUser().getFirstName()));
            Log.d(TAG,"Set Marker for user: "+mRideList.get(i).getVehicle().getUser().getFirstName()+"");
        }
    }
}
