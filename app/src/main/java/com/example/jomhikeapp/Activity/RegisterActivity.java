package com.example.jomhikeapp.Activity;

import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.jomhikeapp.Model.User;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Remote.IMyAPI;
import com.example.jomhikeapp.Remote.RetrofitClient;

import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class RegisterActivity extends AppCompatActivity {
    IMyAPI iMyAPI;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    EditText edt_user_name, edt_password, edt_first_name, edt_last_name, edt_email, edt_phone_number;
    Button btn_create, btn_test;
    private AwesomeValidation awesomeValidation;
    private static final String TAG = "RegisterActivity";

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //INIT API
        iMyAPI = RetrofitClient.getInstance().create(IMyAPI.class);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        //ViewText
        edt_user_name = findViewById(R.id.edt_user_name);
        edt_password = findViewById(R.id.edt_password);
        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_phone_number = findViewById(R.id.edt_phone_number);
        edt_email = findViewById(R.id.edt_email);

        btn_create = findViewById(R.id.btn_create);
        btn_test = findViewById(R.id.btn_test);
        //adding validation to edittexts
        awesomeValidation.addValidation(this, R.id.edt_user_name, "^(?=.{6,8}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", R.string.user_name_error);
        awesomeValidation.addValidation(this, R.id.edt_password, ".{6,}", R.string.password_error);
        awesomeValidation.addValidation(this, R.id.edt_first_name, "[A-Za-z ]+", R.string.first_name_error);
        awesomeValidation.addValidation(this, R.id.edt_last_name, "[A-Za-z ]+", R.string.last_name_error);
        awesomeValidation.addValidation(this, R.id.edt_phone_number, "^(\\+?6?01)[0-46-9]-*[0-9]{7,8}$", R.string.mobile_number_error);
        awesomeValidation.addValidation(this, R.id.edt_email, Patterns.EMAIL_ADDRESS, R.string.email_error);

        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_user_name.setText("ammar97");
                edt_password.setText("sayang");
                edt_first_name.setText("ammar");
                edt_last_name.setText("aziz");
                edt_phone_number.setText("0123456789");
                edt_email.setText("a@gmail.com");
            }
        });
        //Event
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (awesomeValidation.validate()) {
                    final AlertDialog dialog = new SpotsDialog.Builder()
                            .setContext(RegisterActivity.this)
                            .build();
                    dialog.show();

                    User user = new User(
                            edt_user_name.getText().toString(),
                            edt_password.getText().toString(),
                            "",
                            edt_first_name.getText().toString(),
                            edt_last_name.getText().toString(),
                            edt_phone_number.getText().toString(),
                            edt_email.getText().toString());

                    compositeDisposable
                            .add(iMyAPI.registerUser(user)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Consumer<String>() {
                                        @Override
                                        public void accept(String s) throws Exception {
                                            if (s.contains("Successfully")) {
                                                finish();
                                            }
                                            Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }

                                    }, new Consumer<Throwable>() {
                                        @Override
                                        public void accept(Throwable throwable) throws Exception {
                                            dialog.dismiss();
                                            try {
                                                int code = ((HttpException) throwable).code();
//                                                String s = ((HttpException) throwable).message();
                                                String s1 = ((HttpException) throwable).getMessage();
                                                Log.e(TAG, "StatusCode=" + code);
                                                Log.e(TAG, "StatusCode=" + s1);
                                                Toast.makeText(RegisterActivity.this, "The user is existing in database", Toast.LENGTH_SHORT).show();
                                            } catch (Exception e) {
                                                Toast.makeText(RegisterActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }));
                }
            }
        });
    }
}
