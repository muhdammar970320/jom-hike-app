package com.example.jomhikeapp.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.jomhikeapp.Constants;
import com.example.jomhikeapp.Fragment.DatePickerFragment;
import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.Model.Mountain;
import com.example.jomhikeapp.Model.User;
import com.example.jomhikeapp.Model.Vehicle;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Utils.UserSharedPreferences;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class OfferRideActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {
    public static String TAG = "OfferRideActivity";
    private ArrayList<Vehicle> mVehicleList;
    Double mLatitude;
    Double mLongitude;
    Vehicle mVehicle;
    String mSeat;
    String mState;
    String mEventId;
    Event mEvent;
    String mSelectedLocation;
    private RequestQueue mQueue;
    SharedPreferences sharedPreferences;
    ArrayAdapter<Vehicle> adapterVehicleList;
    EditText edt_offer_ride_location_latitude, edt_offer_ride_location_longitude, edt_offer_ride_date_ride;
    ImageButton btn_offer_ride_location, btn_offer_ride_datepicker;
    Button btn_offer_ride;
    private static final int MY_PERMISSION_REQUEST_CODE = 7192;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 7192;
    private FusedLocationProviderClient client;
    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_ride);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        mEvent = intent.getParcelableExtra("Event");

        mSelectedLocation = intent.getStringExtra("selectedLocation");
        client = LocationServices.getFusedLocationProviderClient(this);

        mQueue = Volley.newRequestQueue(this);

        sharedPreferences = getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);
        mVehicleList = new ArrayList<>();
        getVehicleJson();
        VolleyLog.DEBUG = true;
        InitView();
        Validate();

        if(mSelectedLocation!=null){
            String[] latlong =  mSelectedLocation.split(",");
            mLatitude = Double.parseDouble(latlong[0]);
            mLongitude = Double.parseDouble(latlong[1]);
            edt_offer_ride_location_latitude.setText(mLatitude + "");
            edt_offer_ride_location_longitude.setText(mLongitude+"");
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_offer_ride_seat_available:
                mSeat = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinner_offer_ride_state:
                mState = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinner_offer_ride_vehicle_list:
                mVehicle = (Vehicle) parent.getSelectedItem();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //Calendar input form setting
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        edt_offer_ride_date_ride.setText(currentDateString);

    }

    private void getVehicleJson() {
        final Gson gson = new Gson();
        String userId = sharedPreferences.getString(UserSharedPreferences.KEY_USER_ID, null);
        String url = Constants.BASE_URL + "/api/user/" + userId + "/vehicles";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray jsonArray = response.getJSONArray("vehicles");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonVehicles = jsonArray.getJSONObject(i);
                        int id = Integer.parseInt(jsonVehicles.getString("id"));
                        Vehicle vehicle = gson.fromJson(jsonVehicles.toString(), Vehicle.class);
                        Log.d(TAG, jsonVehicles.toString());
//                        Toast.makeText(OfferRideActivity.this, vehicle.getName(), Toast.LENGTH_SHORT).show();

                        mVehicleList.add(vehicle);
                        Log.d(TAG, vehicle.toString());
                    }
                    adapterVehicleList.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void offerRide() {
        String url = Constants.BASE_URL + "/api/ride";
        String date = edt_offer_ride_date_ride.getText().toString();
        String latitude = "" + mLatitude;
        String longitude = "" + mLongitude;
        String eventId = mEvent.getId()+"";
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("rideStartTime", date);
            postparams.put("destinationLongitude", longitude);
            postparams.put("destinationLatitude", "" + latitude);
            postparams.put("seatAvailable", "" + mSeat);
            postparams.put("vehicleId", "" + mVehicle.getId());
            postparams.put("eventId", eventId);
            Log.d(TAG, postparams.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, postparams, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(JSONObject response) {
                    String message = null;
                    try {
                        message = response.getString("message");
                        Log.d(TAG, "Offer Ride successfully ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Offer Ride not success");
                    parseVolleyError(error);
                }
            });
            mQueue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Validate() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_offer_ride_date_ride, RegexTemplate.NOT_EMPTY, R.string.is_required);
        awesomeValidation.addValidation(this, R.id.edt_offer_ride_location_latitude, RegexTemplate.NOT_EMPTY, R.string.is_required);
        awesomeValidation.addValidation(this, R.id.edt_offer_ride_location_longitude, RegexTemplate.NOT_EMPTY, R.string.is_required);
    }

    public void InitView() {
        //Dropdown for seat number
        Spinner spinnerSeatNumber = findViewById(R.id.spinner_offer_ride_seat_available);
        ArrayAdapter<CharSequence> adapterSeatNumber = ArrayAdapter.createFromResource(this, R.array.seat_number, R.layout.spinner_item);
        adapterSeatNumber.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerSeatNumber.setAdapter(adapterSeatNumber);
        spinnerSeatNumber.setOnItemSelectedListener(this);

        //Dropdown for state
        Spinner spinnerStateList = findViewById(R.id.spinner_offer_ride_state);
        ArrayAdapter<CharSequence> adapterStateList = ArrayAdapter.createFromResource(this, R.array.state_in_malaysia, R.layout.spinner_item);
        adapterStateList.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerStateList.setAdapter(adapterStateList);
        spinnerStateList.setOnItemSelectedListener(this);

        //Dropdown for vehicle
        Spinner spinnerVehicleList = findViewById(R.id.spinner_offer_ride_vehicle_list);
        adapterVehicleList =
                new ArrayAdapter<Vehicle>(this, R.layout.spinner_item, mVehicleList);

        adapterVehicleList.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerVehicleList.setAdapter(adapterVehicleList);
        spinnerVehicleList.setOnItemSelectedListener(this);
        btn_offer_ride_location = findViewById(R.id.btn_offer_ride_location);
        edt_offer_ride_date_ride = findViewById(R.id.edt_offer_ride_date_ride);
        edt_offer_ride_location_latitude = findViewById(R.id.edt_offer_ride_location_latitude);
        edt_offer_ride_location_longitude = findViewById(R.id.edt_offer_ride_location_longitude);

        btn_offer_ride_datepicker = findViewById(R.id.btn_offer_ride_datepicker);
        btn_offer_ride = findViewById(R.id.btn_offer_ride);
        btn_offer_ride_datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });
        edt_offer_ride_date_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        btn_offer_ride_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(OfferRideActivity.this, MapsOfferRideActivity.class);
//                        detailIntent.putExtra("eventId", mEventId+"");
                detailIntent.putExtra("Event", mEvent);
                startActivity(detailIntent);
            }
        });
//        edt_offer_ride_date_ride.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                int inType = edt_offer_ride_date_ride.getInputType(); // backup the input type
//                edt_offer_ride_date_ride.setInputType(InputType.TYPE_NULL); // disable soft input
//                edt_offer_ride_date_ride.onTouchEvent(event); // call native handler
//                edt_offer_ride_date_ride.setInputType(inType); // restore input type
//                return true; // consume touch even
//            }
//        });
//        edt_offer_ride_location.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                int inType = edt_offer_ride_location.getInputType(); // backup the input type
//                edt_offer_ride_location.setInputType(InputType.TYPE_NULL); // disable soft input
//                edt_offer_ride_location.onTouchEvent(event); // call native handler
//                edt_offer_ride_location.setInputType(inType); // restore input type
//                return true; // consume touch even
//            }
//        });
        btn_offer_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (awesomeValidation.validate()) {
                    offerRide();
                    Log.d(TAG, "Test");
                }
            }
        });

    }

    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            error.printStackTrace();
            String message = data.getString("message");
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }

    }
    @Override
    public void onBackPressed() {
        Intent detailIntent = new Intent(OfferRideActivity.this, EventDetailActivity.class);
        detailIntent.putExtra("Event", mEvent);
        startActivity(detailIntent);
        finish();
    }
}
