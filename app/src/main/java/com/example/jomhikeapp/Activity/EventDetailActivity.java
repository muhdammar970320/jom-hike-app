package com.example.jomhikeapp.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.jomhikeapp.Constants;
import com.example.jomhikeapp.Fragment.HomeActivity;
import com.example.jomhikeapp.Fragment.HomeFragment;
import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.Model.UserEvent;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Utils.UserSharedPreferences;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class EventDetailActivity extends AppCompatActivity {
    private TextView tv_event_details_name;
    private TextView tv_event_details_location;
    private TextView tv_event_details_state;
    private TextView tv_event_details_organizer_name;
    private TextView tv_event_details_organizer_reg_no;
    private TextView tv_event_details_description;
    private TextView tv_event_details_startdate;
    private TextView tv_event_details_enddate;
    private TextView tv_event_details_mountain;
    private Button btn_join_event, btn_find_ride,btn_offer_ride;
    private Event mEvent;
    private UserEvent userEvent;
    private RequestQueue mQueue;
    public static String TAG ="EventDetailActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        Log.d(TAG,"onCreate()");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        mEvent = intent.getParcelableExtra("Event");

        mQueue = Volley.newRequestQueue(this);
        VolleyLog.DEBUG = true;

        tv_event_details_mountain = findViewById(R.id.tv_event_details_mountain);
        tv_event_details_name = findViewById(R.id.tv_event_details_name);
        tv_event_details_location = findViewById(R.id.tv_event_details_location);
        tv_event_details_state = findViewById(R.id.tv_event_details_state);
        tv_event_details_organizer_name = findViewById(R.id.tv_event_details_organizer_name);
        tv_event_details_organizer_reg_no = findViewById(R.id.tv_event_details_organizer_reg_no);
        tv_event_details_description = findViewById(R.id.tv_event_details_description);
        tv_event_details_startdate = findViewById(R.id.tv_event_details_startdate);
        tv_event_details_enddate = findViewById(R.id.tv_event_details_enddate);
        btn_join_event = findViewById(R.id.btn_join_event);
        btn_find_ride = findViewById(R.id.btn_find_ride);
        btn_offer_ride = findViewById(R.id.btn_offer_ride);

        tv_event_details_mountain.setText(mEvent.getMountain().getName());
        tv_event_details_name.setText(mEvent.getName());
        tv_event_details_location.setText(mEvent.getLocation());
        tv_event_details_state.setText(mEvent.getState());
        tv_event_details_organizer_name.setText(mEvent.getOrganizerName());
        tv_event_details_organizer_reg_no.setText(mEvent.getOrganizerRegistrationNumber());
        tv_event_details_description.setText(mEvent.getDescription());
        tv_event_details_startdate.setText(mEvent.getStartDate().toString());
        tv_event_details_enddate.setText(mEvent.getEndDate().toString());
        getUserEvent(mEvent);
        btn_join_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_join_event:
                        if(btn_join_event.getText().toString().equalsIgnoreCase("join"))
                            //Option 1 means join the event
                            alertDialog(1);
                        else
                            //Option 2 means cancel the event
                            alertDialog(2);
                        break;
                }
            }
        });
        btn_offer_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_offer_ride:
                        Intent detailIntent = new Intent(EventDetailActivity.this, OfferRideActivity.class);
                        detailIntent.putExtra("Event", mEvent);
                        startActivity(detailIntent);
                        break;
                }
            }
        });
        btn_find_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_find_ride:
                        Intent detailIntent = new Intent(EventDetailActivity.this, FindRideActivity.class);
                        detailIntent.putExtra("eventId", mEvent.getId()+"");
                        startActivity(detailIntent);
                        break;
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void alertDialog(int option) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Confirmation");
        // 1 for join
        if(option==1) {
            dialog.setMessage("Are you confirm to join this event?");
            dialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            joinEvent(mEvent);
                        }
                    });
            dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
        // 2 for cancel
        else{
            dialog.setMessage("Are you confirm to cancel this event?");
            dialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            cancelEvent(mEvent);
                        }
                    });
            dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }
    private void joinEvent(final Event event) {
        SharedPreferences sharedPreferences = getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("userId",sharedPreferences.getString(UserSharedPreferences.KEY_USER_ID, null));
            postparams.put("eventId",""+event.getId());


        String url = Constants.BASE_URL + "/api/event/join";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,postparams, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                String message = null;
                try {
                    message = response.getString("message");
                    Log.d(TAG,"Event Joined Successfully");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(),message , Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                parseVolleyError(error);
                Log.e(TAG,"Event Joined Not Success");
            }
        });
        mQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void cancelEvent(final Event event) {
        SharedPreferences sharedPreferences = getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("userId",sharedPreferences.getString(UserSharedPreferences.KEY_USER_ID, null));
            postparams.put("eventId",""+event.getId());

            String url = Constants.BASE_URL + "/api/event/cancel";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,postparams, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(JSONObject response) {
                    String message = null;
                    try {
                        message = response.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(),message , Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    parseVolleyError(error);
                }
            });
            mQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            error.printStackTrace();
            String message = data.getString("message");
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

    private void getUserEvent(Event event) {
        SharedPreferences sharedPreferences = getSharedPreferences(UserSharedPreferences.MY_PREFERENCES, Context.MODE_PRIVATE);

        String url = Constants.BASE_URL + "/api/event/"+event.getId()+"/user/"+sharedPreferences.getString(UserSharedPreferences.KEY_USER_ID,null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                String message = null;
                //Get data in UserEvent table in NetCore
                try {
                    Gson gson = new Gson();
                    userEvent = gson.fromJson(response.toString(), UserEvent.class);
                    setButtonToggleJoin(userEvent);
                    setDisabledButton(userEvent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                userEvent=null;
                parseVolleyError(error);
            }
        });
        mQueue.add(request);
    }
    public void setButtonToggleJoin(UserEvent userEvent){
         //Check if attempt not exceed than 3
        if (userEvent.getMaxAttempt() < 3) {
            if(userEvent.getStatus().equalsIgnoreCase("joined")){
                btn_join_event.setText("Cancel");
            }
            else{
                btn_join_event.setText("Join");
            }
        }
        else {
            //if user have made 3 times attempt join event, it will block user from joined
            //in 4th times
            String message="You have reach maximum attempt to join this event. Please contact user support for details";
            btn_join_event.setEnabled(false);
            btn_find_ride.setEnabled(false);
            btn_offer_ride.setEnabled(false);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }
    public void setDisabledButton(UserEvent userEvent){
        //Disabled button findRide if user already offerRide
        if (userEvent.getIsOfferRide() ==1) {
            btn_find_ride.setEnabled(false);
        }
        else{
            btn_find_ride.setEnabled(true);
        }
        //Disabled button offerRide if user already requestRide
        if (userEvent.getIsRequestRide() ==1) {
            btn_offer_ride.setEnabled(false);
        }
        else{
            btn_offer_ride.setEnabled(true);
        }
    }
    @Override
    public void onBackPressed() {
        Intent detailIntent = new Intent(EventDetailActivity.this, HomeActivity.class);
        startActivity(detailIntent);
        finish();
    }
}
