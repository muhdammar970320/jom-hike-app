package com.example.jomhikeapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jomhikeapp.Model.Ride;
import com.example.jomhikeapp.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RideAdapter  extends RecyclerView.Adapter<RideAdapter.RideViewHolder> {
    private ArrayList<Ride> mRideList;
    private Context mContext;
    private LayoutInflater inflater;
    private RideAdapter.OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
    public void setRide(ArrayList<Ride> rideList){
        mRideList = rideList;
        notifyDataSetChanged();
    }
    public static class RideViewHolder extends  RecyclerView.ViewHolder {
        TextView tv_card_ride_user_name;
        TextView tv_card_ride_seat_available;
        TextView tv_card_ride_total_distance;
        TextView tv_card_ride_state;
        TextView tv_card_ride_type_car;
        ImageView iv_ic_profile;

        public RideViewHolder(@NonNull View itemView, final RideAdapter.OnItemClickListener listener) {
            super(itemView);

            tv_card_ride_user_name = itemView.findViewById(R.id.tv_card_ride_user_name);
            tv_card_ride_seat_available = itemView.findViewById(R.id.tv_card_ride_seat_available);
            tv_card_ride_total_distance = itemView.findViewById(R.id.tv_card_ride_total_distance);
            tv_card_ride_state = itemView.findViewById(R.id.tv_card_ride_state);
            tv_card_ride_type_car = itemView.findViewById(R.id.tv_card_ride_type_car);
            iv_ic_profile = itemView.findViewById(R.id.ic_profile);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
    public RideAdapter(Context context, ArrayList<Ride> ridesList) {
        mRideList = ridesList;
        this.mContext=context;
        inflater= LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public RideViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = inflater.inflate(R.layout.ride_item, viewGroup, false);
        RideAdapter.RideViewHolder evh =  new RideViewHolder(v, mListener);
        return evh;
    }
    @Override
    public void onBindViewHolder(@NonNull RideViewHolder rideViewHolder, int i) {
        Ride currentItem = mRideList.get(i);
        rideViewHolder.iv_ic_profile.setImageResource(R.drawable.ic_profile);
        rideViewHolder.tv_card_ride_user_name.setText(currentItem.getVehicle().getUser().getFirstName()+" "+currentItem.getVehicle().getUser().getLastName());
        rideViewHolder.tv_card_ride_seat_available.setText(currentItem.getSeatAvailable()+"");
        rideViewHolder.tv_card_ride_total_distance.setText(currentItem.getTotalDistance()+"");
        rideViewHolder.tv_card_ride_state.setText("Dummy State");
        rideViewHolder.tv_card_ride_type_car.setText(currentItem.getVehicle().getName()+"");
    }
    @Override
    public int getItemCount() {
        return mRideList.size();
    }

}
