package com.example.jomhikeapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.R;

import java.util.ArrayList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {
    private ArrayList<Event> mEventsList;
    private Context mContext;
    private LayoutInflater inflater;
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
    public void setEvent(ArrayList<Event> eventList) {
        mEventsList = eventList;
        notifyDataSetChanged();
    }
    public static class EventViewHolder extends RecyclerView.ViewHolder{
        public TextView mEventName;
        public TextView mEventLocation;
        public TextView mState;
        public TextView mEventDate;
        public TextView mEventOrganizeBy;
        public TextView mEventOrganizer;
        public EventViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            mEventName = itemView.findViewById(R.id.tv_card_event_name);
            mEventLocation = itemView.findViewById(R.id.tv_card_event_location);
            mState = itemView.findViewById(R.id.tv_card_event_state);
            mEventDate = itemView.findViewById(R.id.tv_card_event_date);
            mEventOrganizer = itemView.findViewById(R.id.tv_card_event_organizer);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
    public EventAdapter(Context context, ArrayList<Event> eventsList) {
        mEventsList = eventsList;
        this.mContext=context;
        inflater= LayoutInflater.from(context);

}

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = inflater.inflate(R.layout.event_item, viewGroup, false);
        EventAdapter.EventViewHolder evh =  new EventViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder eventViewHolder, int i) {
        Event currentItem = mEventsList.get(i);
        eventViewHolder.mEventName.setText(currentItem.getName());
        eventViewHolder.mEventLocation.setText(currentItem.getLocation());
        eventViewHolder.mState.setText(currentItem.getState());
        eventViewHolder.mEventDate.setText(currentItem.getStartDate().toString());
        eventViewHolder.mEventOrganizer.setText(currentItem.getOrganizerName());
    }

    @Override
    public int getItemCount() {
        return mEventsList.size();
    }

}
