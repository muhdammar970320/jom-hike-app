package com.example.jomhikeapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {
    public static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static final String BASE_URL = "http://192.168.0.185:5000";
//public static final String BASE_URL = "http://10.82.11.41:5000";

//public static final String BASE_URL = "https://jomhikeapiv2.herokuapp.com";
}
