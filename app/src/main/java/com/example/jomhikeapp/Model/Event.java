package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

public class Event implements Parcelable {
    @SerializedName("id")
    private int Id;

    @SerializedName("name")
    private String Name;

    @SerializedName("location")
    private String Location;

    @SerializedName("state")
    private String State;

    @SerializedName("organizerName")
    private String OrganizerName;

    @SerializedName("organizerRegistrationNumber")
    private String OrganizerRegistrationNumber;

    @SerializedName("description")
    private String Description;

    @SerializedName("startDate")
    private Date StartDate;

    @SerializedName("endDate")
    private Date EndDate;

    @SerializedName("status")
    private String Status;

    @SerializedName("userId")
    private UUID UserId;

    //    @SerializedName("mountainId")
//    private int MountainId;
    @SerializedName("mountain")
    private Mountain Mountain;


    public Event(int id, String name, String location, String state, String organizerName, String organizerRegistrationNumber, String description, Date startDate, Date endDate, String status, UUID userId, Mountain mountain) {
        Id = id;
        Name = name;
        Location = location;
        State = state;
        OrganizerName = organizerName;
        OrganizerRegistrationNumber = organizerRegistrationNumber;
        Description = description;
        StartDate = startDate;
        EndDate = endDate;
        Status = status;
        UserId = userId;
        Mountain = mountain;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getOrganizerName() {
        return OrganizerName;
    }

    public void setOrganizerName(String organizerName) {
        OrganizerName = organizerName;
    }

    public String getOrganizerRegistrationNumber() {
        return OrganizerRegistrationNumber;
    }

    public void setOrganizerRegistrationNumber(String organizerRegistrationNumber) {
        OrganizerRegistrationNumber = organizerRegistrationNumber;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public UUID getUserId() {
        return UserId;
    }

    public void setUserId(UUID userId) {
        UserId = userId;
    }


    public void changeOrganizerName(String value) {
        OrganizerName = value;
    }

    public com.example.jomhikeapp.Model.Mountain getMountain() {
        return Mountain;
    }

    public void setMountain(com.example.jomhikeapp.Model.Mountain mountain) {
        Mountain = mountain;
    }

    @Override
    public String toString() {
        return "Event{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Location='" + Location + '\'' +
                ", State='" + State + '\'' +
                ", OrganizerName='" + OrganizerName + '\'' +
                ", OrganizerRegistrationNumber='" + OrganizerRegistrationNumber + '\'' +
                ", Description='" + Description + '\'' +
                ", StartDate=" + StartDate +
                ", EndDate=" + EndDate +
                ", Status='" + Status + '\'' +
                ", UserId=" + UserId +
//                ", MountainId=" + MountainId +
                '}';
    }

    protected Event(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        Location = in.readString();
        State = in.readString();
        OrganizerName = in.readString();
        OrganizerRegistrationNumber = in.readString();
        Description = in.readString();
        long tmpStartDate = in.readLong();
        StartDate = tmpStartDate != -1 ? new Date(tmpStartDate) : null;
        long tmpEndDate = in.readLong();
        EndDate = tmpEndDate != -1 ? new Date(tmpEndDate) : null;
        Status = in.readString();
        UserId = (UUID) in.readValue(UUID.class.getClassLoader());
        Mountain = (Mountain) in.readValue(Mountain.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeString(Location);
        dest.writeString(State);
        dest.writeString(OrganizerName);
        dest.writeString(OrganizerRegistrationNumber);
        dest.writeString(Description);
        dest.writeLong(StartDate != null ? StartDate.getTime() : -1L);
        dest.writeLong(EndDate != null ? EndDate.getTime() : -1L);
        dest.writeString(Status);
        dest.writeValue(UserId);
        dest.writeValue(Mountain);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}