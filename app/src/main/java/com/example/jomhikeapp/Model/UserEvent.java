package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class UserEvent implements Parcelable {
    @SerializedName("id")
    private int Id;

    @SerializedName("isRequestRide")
    private int IsRequestRide;

    @SerializedName("isOfferRide")
    private int IsOfferRide;

    @SerializedName("maxAttempt")
    private int MaxAttempt;

    @SerializedName("status")
    private String Status;

    @SerializedName("userId")
    private UUID UserId;

    @SerializedName("eventId")
    private int EventId;

    public UserEvent(int id, int isRequestRide, int isOfferRide, int maxAttempt, String status, UUID userId, int eventId) {
        Id = id;
        IsRequestRide = isRequestRide;
        IsOfferRide = isOfferRide;
        MaxAttempt = maxAttempt;
        Status = status;
        UserId = userId;
        EventId = eventId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getIsRequestRide() {
        return IsRequestRide;
    }

    public void setIsRequestRide(int isRequestRide) {
        IsRequestRide = isRequestRide;
    }

    public int getIsOfferRide() {
        return IsOfferRide;
    }

    public void setIsOfferRide(int isOfferRide) {
        IsOfferRide = isOfferRide;
    }

    public int getMaxAttempt() {
        return MaxAttempt;
    }

    public void setMaxAttempt(int maxAttempt) {
        MaxAttempt = maxAttempt;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public UUID getUserId() {
        return UserId;
    }

    public void setUserId(UUID userId) {
        UserId = userId;
    }

    public int getEventId() {
        return EventId;
    }

    public void setEventId(int eventId) {
        EventId = eventId;
    }

    @Override
    public String toString() {
        return "UserEvent{" +
                "Id=" + Id +
                ", IsRequestRide=" + IsRequestRide +
                ", IsOfferRide=" + IsOfferRide +
                ", MaxAttempt=" + MaxAttempt +
                ", Status='" + Status + '\'' +
                ", UserId=" + UserId +
                ", EventId=" + EventId +
                '}';
    }

    protected UserEvent(Parcel in) {
        Id = in.readInt();
        IsRequestRide = in.readInt();
        IsOfferRide = in.readInt();
        MaxAttempt = in.readInt();
        Status = in.readString();
        UserId = (UUID) in.readValue(UUID.class.getClassLoader());
        EventId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(IsRequestRide);
        dest.writeInt(IsOfferRide);
        dest.writeInt(MaxAttempt);
        dest.writeString(Status);
        dest.writeValue(UserId);
        dest.writeInt(EventId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserEvent> CREATOR = new Parcelable.Creator<UserEvent>() {
        @Override
        public UserEvent createFromParcel(Parcel in) {
            return new UserEvent(in);
        }

        @Override
        public UserEvent[] newArray(int size) {
            return new UserEvent[size];
        }
    };
}