package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

public class User implements Parcelable {
    @SerializedName("id")
    public UUID Id;

    @SerializedName("userName")
    private String UserName;

    @SerializedName("password")
    private String Password;

    @SerializedName("salt")
    private String Salt;

    @SerializedName("firstName")
    private String FirstName;

    @SerializedName("lastName")
    private String LastName;

    @SerializedName("identityNumber")
    private String IdentityNumber;

    @SerializedName("phoneNumber")
    private String PhoneNumber;

    @SerializedName("address1")
    private String Address1;

    @SerializedName("address2")
    private String Address2;

    @SerializedName("postalCode")
    private String PostalCode;

    @SerializedName("role")
    private String Role;

    @SerializedName("email")
    private String Email;

    @SerializedName("status")
    private int Status;

    @SerializedName("driverLicenseNumber")
    private String DriverLicenseNumber;

    @SerializedName("driverLicenseExpiredDate")
    private Date DriverLicenseExpiredDate;


    public User() {
    }

    public User(String userName, String password, String salt, String firstName, String lastName, String phoneNumber, String email) {
        UserName = userName;
        Password = password;
        Salt = salt;
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        Email = email;
    }

    public User(String userName, String password, String salt, String firstName, String lastName, String identityNumber, String address1, String address2, String postalCode, String role, String email, int status, String driverLicenseNumber, Date driverLicenseExpiredDate) {
        UserName = userName;
        Password = password;
        Salt = salt;
        FirstName = firstName;
        LastName = lastName;
        IdentityNumber = identityNumber;
        Address1 = address1;
        Address2 = address2;
        PostalCode = postalCode;
        Role = role;
        Email = email;
        Status = status;
        DriverLicenseNumber = driverLicenseNumber;
        DriverLicenseExpiredDate = driverLicenseExpiredDate;
    }

    public User(String userName, String password, String salt) {
        UserName = userName;
        Password = password;
        Salt = salt;
    }

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        Id = id;
    }
    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getIdentityNumber() {
        return IdentityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        IdentityNumber = identityNumber;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getDriverLicenseNumber() {
        return DriverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        DriverLicenseNumber = driverLicenseNumber;
    }

    public Date getDriverLicenseExpiredDate() {
        return DriverLicenseExpiredDate;
    }

    public void setDriverLicenseExpiredDate(Date driverLicenseExpiredDate) {
        DriverLicenseExpiredDate = driverLicenseExpiredDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "UserName='" + UserName + '\'' +
                ", Password='" + Password + '\'' +
                ", Salt='" + Salt + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", IdentityNumber='" + IdentityNumber + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", Address1='" + Address1 + '\'' +
                ", Address2='" + Address2 + '\'' +
                ", PostalCode='" + PostalCode + '\'' +
                ", Role='" + Role + '\'' +
                ", Email='" + Email + '\'' +
                ", Status=" + Status +
                ", DriverLicenseNumber='" + DriverLicenseNumber + '\'' +
                ", DriverLicenseExpiredDate=" + DriverLicenseExpiredDate +
                '}';
    }
    protected User(Parcel in) {
        Id = (UUID) in.readValue(UUID.class.getClassLoader());
        UserName = in.readString();
        Password = in.readString();
        Salt = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
        IdentityNumber = in.readString();
        PhoneNumber = in.readString();
        Address1 = in.readString();
        Address2 = in.readString();
        PostalCode = in.readString();
        Role = in.readString();
        Email = in.readString();
        Status = in.readInt();
        DriverLicenseNumber = in.readString();
        long tmpDriverLicenseExpiredDate = in.readLong();
        DriverLicenseExpiredDate = tmpDriverLicenseExpiredDate != -1 ? new Date(tmpDriverLicenseExpiredDate) : null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Id);
        dest.writeString(UserName);
        dest.writeString(Password);
        dest.writeString(Salt);
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(IdentityNumber);
        dest.writeString(PhoneNumber);
        dest.writeString(Address1);
        dest.writeString(Address2);
        dest.writeString(PostalCode);
        dest.writeString(Role);
        dest.writeString(Email);
        dest.writeInt(Status);
        dest.writeString(DriverLicenseNumber);
        dest.writeLong(DriverLicenseExpiredDate != null ? DriverLicenseExpiredDate.getTime() : -1L);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
