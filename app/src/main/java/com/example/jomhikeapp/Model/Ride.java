package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Ride implements Parcelable {
    @SerializedName("id")
    private int Id;

    @SerializedName("rideStartTime")
    private Date RideStartTime;

    @SerializedName("destinationLongitude")
    private Double DestinationLongitude;

    @SerializedName("destinationLatitude")
    private Double DestinationLatitude;

    @SerializedName("seatAvailable")
    private int SeatAvailable;

    @SerializedName("totalCost")
    private Double TotalCost;

    @SerializedName("createdAt")
    private Date CreatedAt;

    @SerializedName("totalDistance")
    private Double TotalDistance;

    @SerializedName("vehicle")
    private Vehicle Vehicle;

    @SerializedName("event")
    private Event Event;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Date getRideStartTime() {
        return RideStartTime;
    }

    public void setRideStartTime(Date rideStartTime) {
        RideStartTime = rideStartTime;
    }

    public Double getDestinationLongitude() {
        return DestinationLongitude;
    }

    public void setDestinationLongitude(Double destinationLongitude) {
        DestinationLongitude = destinationLongitude;
    }

    public Double getDestinationLatitude() {
        return DestinationLatitude;
    }

    public void setDestinationLatitude(Double destinationLatitude) {
        DestinationLatitude = destinationLatitude;
    }

    public int getSeatAvailable() {
        return SeatAvailable;
    }

    public void setSeatAvailable(int seatAvailable) {
        SeatAvailable = seatAvailable;
    }

    public Double getTotalCost() {
        return TotalCost;
    }

    public void setTotalCost(Double totalCost) {
        TotalCost = totalCost;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Double getTotalDistance() {
        return TotalDistance;
    }

    public void setTotalDistance(Double totalDistance) {
        TotalDistance = totalDistance;
    }

    public com.example.jomhikeapp.Model.Vehicle getVehicle() {
        return Vehicle;
    }

    public void setVehicle(com.example.jomhikeapp.Model.Vehicle vehicle) {
        Vehicle = vehicle;
    }

    public com.example.jomhikeapp.Model.Event getEvent() {
        return Event;
    }

    public void setEvent(com.example.jomhikeapp.Model.Event event) {
        Event = event;
    }

    public Ride(Parcel in) {
        Id = in.readInt();
        long tmpRideStartTime = in.readLong();
        RideStartTime = tmpRideStartTime != -1 ? new Date(tmpRideStartTime) : null;
        DestinationLongitude = in.readByte() == 0x00 ? null : in.readDouble();
        DestinationLatitude = in.readByte() == 0x00 ? null : in.readDouble();
        SeatAvailable = in.readInt();
        TotalCost = in.readByte() == 0x00 ? null : in.readDouble();
        long tmpCreatedAt = in.readLong();
        CreatedAt = tmpCreatedAt != -1 ? new Date(tmpCreatedAt) : null;
        TotalDistance = in.readByte() == 0x00 ? null : in.readDouble();
        Vehicle = (Vehicle) in.readValue(Vehicle.class.getClassLoader());
        Event = (Event) in.readValue(Event.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeLong(RideStartTime != null ? RideStartTime.getTime() : -1L);
        if (DestinationLongitude == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(DestinationLongitude);
        }
        if (DestinationLatitude == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(DestinationLatitude);
        }
        dest.writeInt(SeatAvailable);
        if (TotalCost == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(TotalCost);
        }
        dest.writeLong(CreatedAt != null ? CreatedAt.getTime() : -1L);
        if (TotalDistance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(TotalDistance);
        }
        dest.writeValue(Vehicle);
        dest.writeValue(Event);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Ride> CREATOR = new Parcelable.Creator<Ride>() {
        @Override
        public Ride createFromParcel(Parcel in) {
            return new Ride(in);
        }

        @Override
        public Ride[] newArray(int size) {
            return new Ride[size];
        }
    };
}