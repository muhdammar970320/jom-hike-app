package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

public class Mountain implements Parcelable {
    private int Id;
    private String Name;
    private int HeightFeet;
    private int HeightMeter;
    private String State;
    private Double AverageDifficulty;
    private Double AverageRating;

    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
    public Mountain() {}
    public Mountain(int id, String name, int heightFeet, int heightMeter, String state, Double averageDifficulty, Double averageRating) {
        Id = id;
        Name = name;
        HeightFeet = heightFeet;
        HeightMeter = heightMeter;
        State = state;
        AverageDifficulty = averageDifficulty;
        AverageRating = averageRating;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getHeightFeet() {
        return HeightFeet;
    }

    public void setHeightFeet(int heightFeet) {
        HeightFeet = heightFeet;
    }

    public int getHeightMeter() {
        return HeightMeter;
    }

    public void setHeightMeter(int heightMeter) {
        HeightMeter = heightMeter;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public Double getAverageDifficulty() {
        return AverageDifficulty;
    }

    public void setAverageDifficulty(Double averageDifficulty) {
        AverageDifficulty = averageDifficulty;
    }

    public Double getAverageRating() {
        return AverageRating;
    }

    public void setAverageRating(Double averageRating) {
        AverageRating = averageRating;
    }

    protected Mountain(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        HeightFeet = in.readInt();
        HeightMeter = in.readInt();
        State = in.readString();
        AverageDifficulty = in.readByte() == 0x00 ? null : in.readDouble();
        AverageRating = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeInt(HeightFeet);
        dest.writeInt(HeightMeter);
        dest.writeString(State);
        if (AverageDifficulty == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(AverageDifficulty);
        }
        if (AverageRating == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(AverageRating);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Mountain> CREATOR = new Parcelable.Creator<Mountain>() {
        @Override
        public Mountain createFromParcel(Parcel in) {
            return new Mountain(in);
        }

        @Override
        public Mountain[] newArray(int size) {
            return new Mountain[size];
        }
    };
}