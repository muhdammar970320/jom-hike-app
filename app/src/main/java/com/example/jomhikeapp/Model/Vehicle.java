package com.example.jomhikeapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class Vehicle implements Parcelable {
    @SerializedName("id")
    private int Id;

    @SerializedName("name")
    private String Name;

    @SerializedName("brand")
    private String Brand;

    @SerializedName("makeYear")
    private int MakeYear;

    @SerializedName("color")
    private String Color;

    @SerializedName("plateNumber")
    private String PlateNumber;

    @SerializedName("userId")
    private UUID UserId;
    @SerializedName("user")
    private User user;

    @Override
    public String toString() {
        return getName();
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public int getMakeYear() {
        return MakeYear;
    }

    public void setMakeYear(int makeYear) {
        MakeYear = makeYear;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getPlateNumber() {
        return PlateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        PlateNumber = plateNumber;
    }

    public UUID getUserId() {
        return UserId;
    }

    public void setUserId(UUID userId) {
        UserId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static Creator<Vehicle> getCREATOR() {
        return CREATOR;
    }

    public Vehicle(int id, String name, String brand, int makeYear, String color, String plateNumber, UUID userId, User user) {
        Id = id;
        Name = name;
        Brand = brand;
        MakeYear = makeYear;
        Color = color;
        PlateNumber = plateNumber;
        UserId = userId;
        this.user = user;
    }

    protected Vehicle(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        Brand = in.readString();
        MakeYear = in.readInt();
        Color = in.readString();
        PlateNumber = in.readString();
        UserId = (UUID) in.readValue(UUID.class.getClassLoader());
        user = (User) in.readValue(User.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeString(Brand);
        dest.writeInt(MakeYear);
        dest.writeString(Color);
        dest.writeString(PlateNumber);
        dest.writeValue(UserId);
        dest.writeValue(user);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Vehicle> CREATOR = new Parcelable.Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}