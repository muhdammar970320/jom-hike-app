package com.example.jomhikeapp.Test;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jomhikeapp.Model.ExampleItem;
import com.example.jomhikeapp.R;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        ArrayList<ExampleItem> exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));
        exampleList.add(new ExampleItem(R.drawable.ic_info,"Line 2","Line 3"));
        exampleList.add(new ExampleItem(R.drawable.ic_logout,"Line 4","Line 5"));
        exampleList.add(new ExampleItem(R.drawable.home_logo,"Line 6","Line 7"));

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new TestAdapter(exampleList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
