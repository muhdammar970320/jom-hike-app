package com.example.jomhikeapp.Test;

import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.jomhikeapp.Adapter.EventAdapter;
import com.example.jomhikeapp.Constants;
import com.example.jomhikeapp.Model.Event;
import com.example.jomhikeapp.Model.Mountain;
import com.example.jomhikeapp.R;
import com.example.jomhikeapp.Remote.IMyAPI;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import io.reactivex.disposables.CompositeDisposable;

public class TestRecylerView extends AppCompatActivity {
    IMyAPI iMyAPI;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    View rootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RequestQueue mQueue;
    public static String TAG = "HomeFragment";
    private ArrayList<Event> eventsList;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_test_recyler_view);
//    }


    private void jsonParse() {

        String url = Constants.BASE_URL+"/api/event";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray jsonArray = response.getJSONArray("events");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject event = jsonArray.getJSONObject(i);
                        int Id = event.getInt("id");
                        String Name = event.getString("name");
                        String Location = event.getString("location");
                        String State = event.getString("state");
                        String OrganizerName = event.getString("organizerName");
                        String OrganizerRegistrationNumber = event.getString("organizerRegistrationNumber");
                        String Description = event.getString("description");
                        Date StartDate = Constants.dateFormat.parse(event.getString("startDate"));
                        Date EndDate =   Constants.dateFormat.parse(event.getString("endDate"));
                        String Status = event.getString("status");
                        UUID UserId = UUID.fromString(event.getString("userId"));
//                        int MountainId =event.getInt("mountainId");
                        Mountain mountain = new Mountain();
                        Event eventClass = new Event(Id, Name,Location,State,OrganizerName ,OrganizerRegistrationNumber ,Description,StartDate,EndDate ,Status ,UserId, mountain);
                        eventsList.add(eventClass);
//                        Toast.makeText(getActivity(), Name, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, eventClass.toString());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_recyler_view);
        eventsList = new ArrayList<>();
        mQueue = Volley.newRequestQueue(this);
        jsonParse();

        Log.d(TAG, "Restart view");

//        eventsList = new ArrayList<>();
        mRecyclerView = findViewById(R.id.eventTestRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);

        mAdapter = new EventAdapter(this,eventsList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        Log.d(TAG, "OnCreated");



    }
}
