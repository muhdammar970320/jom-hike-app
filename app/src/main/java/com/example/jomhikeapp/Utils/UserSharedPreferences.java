package com.example.jomhikeapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.jomhikeapp.Activity.MainActivity;

public class UserSharedPreferences {
    SharedPreferences pref;
    public static final String MY_PREFERENCES = "UserSession";

    public static final String IS_USER_LOGIN = "IsUserLoggedIn";

    public static final String KEY_NAME = "Name";

    public static final String KEY_USER_ID = "UserId";

    public static final String KEY_EMAIL = "Email";

    public static final String KEY_PASSWORD = "Password";

    public static final String KEY_FNAME = "FirstName";

    public static final String KEY_LNAME = "LastName";

    public static final String KEY_PHONE = "PhoneNumber";

    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared preferences mode
    int PRIVATE_MODE = 0;

    //Create login session
    public void createUserLoginSession(String userId, String firstName, String lastName, String phoneNumber, String email, String uName, String uPassword) {
        // Storing login value as TRUE
        editor = pref.edit();
        editor.putBoolean(IS_USER_LOGIN, true);

        editor.putString(KEY_USER_ID, userId);

        editor.putString(KEY_NAME, uName);

        editor.putString(KEY_FNAME, firstName);

        editor.putString(KEY_LNAME, lastName);

        editor.putString(KEY_PHONE, phoneNumber);

        editor.putString(KEY_EMAIL, email);

        editor.putString(KEY_PASSWORD, uPassword);

        // commit changes
        editor.commit();
    }

    public UserSharedPreferences(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(MY_PREFERENCES, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean checkLogin() {
        // Check login status
        if (!this.isUserLoggedIn()) {
            return false;
        }
        return true;
    }

    public void logoutUser() {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to MainActivity
        Intent i = new Intent(_context, MainActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    // Check for login
    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
}

